% This example analyses dataset 2, available at
% http://bnci-horizon-2020.eu/database/data-sets
% # 22. Monitoring error-related potentials (013-2015)
% using the approach of the paper to train on session 1 and test session 2 
% In the experiment, error-related potentials are investigated. Subjects
% observed a cursor movement of an agent
%
% this is a binary single trial classification problem, so we have no
% sequences of stimuli and no targets. Labels are directly calculated in L
% and must not be calculated internally by the ERPDecoder (usually from Y and T)  
function [accuracy, recall] = analyze_sov_dataset4( datapath, verbose)

if nargin ==0
    datapath='../../data/dataset4/';
end

if nargin <2
    verbose = true;
end

ComponentLimit = 4; % limit the number of components to 4

Classifier = 'lda'; % use 'lda' to determine targets
% could also be done with 'svm' 'maxcorr' or 'naivebayes'

FeatureSpace = 'canonical'; % 'maxcorr' classifier requires 'correlation'
% features but other classifiers could also use 'canonical'

ReferenceType = 'impulse'; % could also be 'impulse'; 'average' might be
% advantageous here because we have much less sensors compared to samples

flist{1} = dir([ datapath 'Subject*s1.mat']);
flist{2} = dir([ datapath 'Subject*s2.mat']);


if isempty( flist{1}) || length(flist{1})~=length(flist{2})
    error('Not all data files found.');
end

nSubj = length( flist{1});

accuracy = zeros(nSubj,1);
recall = zeros(nSubj,2);

for sk = 1: nSubj % loop over subjects
    % load 1st session
    dat = load ([ datapath flist{1}( sk).name]);

    % preprocess training data: segment, filter and downsample data; define labels 
    [ epochs,L,meta] = preprocess_dataset4( dat);

    % train classifier
    Mdl{1} = fiterpcca( epochs, L, [], [],...            
            'ComponentLimit', ComponentLimit,...
            'ReferenceType',ReferenceType, ...
            'Classifier', Classifier,...
            'FeatureSpace', FeatureSpace);
    
    % load 2nd session
    dat = load ([ datapath flist{2}( sk).name]);

    % preprocess test data: segment, filter and downsample data; define labels 
    [ epochs,L,meta] = preprocess_dataset4( dat);
    predID = Mdl{1}.predict(epochs);
    
    accuracy(sk) = sum(L==predID)/length(L);
    recall(sk,1) = sum( predID==L & L==1) / sum(L==1);
    recall(sk,2) = sum( predID==L & L==2) / sum(L==2);
    if verbose        
        fprintf('Subj %i:  %3.0f%% (%3.0f%%/%3.0f%%)\n',...
                sk, accuracy(sk)*100, recall(sk,:)*100);
    end
end
if verbose
    fprintf( 'Average accuracy: %3.1f%% (std: %2.1f%%)\n',...
        mean(accuracy*100),std(accuracy*100));
    fprintf( 'Average recall: %3.1f%%/%3.1f%% (std: %2.1f%%/%2.1f%%)\n',...
        mean(recall*100,1),std(recall*100,[],1));
    % the algorithm does not generalize well across sessions !!!
end
