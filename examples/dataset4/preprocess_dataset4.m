function [epochs,L, meta] = preprocess_dataset4( D)
% [epochs,L, meta] = preprocess_dataset2( D)
% preprocesses data of dataset
% 22. Monitoring error-related potentials (013-2015)
% from http://bnci-horizon-2020.eu/database/data-sets
% corresponding to publication
% http://dx.doi.org/10.1109/TNSRE.2010.2053387
% only the P_err=0.2 blocks are present in this data set
%
% this is a binary single trial classification problem, so we have no
% sequences of stimuli and no targets. Labels are directly calculated in L
% and must not be calculated internally by the ERPDecoder (usually from stimID and trgtID)  


filtbuf = 1.0; % buffer pre and post interval to prevent filter edge artifacts
filtorder = 4;
passband = [0.1, 10.0];
resampfreq = 32;
winsize = 0.6;
baselinelength = 0.25;
meta.Label = D.run{ 1}.header.Label;

nTrialsPerBlock = zeros( size(D.run));
fs = D.run{ 1}.header.SampleRate;

% filter coefficients
[butterB, butterA] = butter( filtorder/2, passband /(fs/2), 'bandpass');

% resample parameters
[P,Q]=rat(resampfreq / fs);

intervalStim = 0:round( winsize * fs);
baselineInterval = -round( baselinelength * fs):0;
interval = round( -filtbuf * fs): round((winsize+filtbuf) * fs);
onset = find(interval>=0,1);

for blockCount = 1:length(D.run)
    [~,evtpos]=ismember( D.run{ blockCount}.header.EVENT.TYP',[5,6,9,10]);
    nTrialsPerBlock( blockCount) = sum( evtpos>0);
end

% initialze epochs (nChan x nSamp x nStim)
epochs = zeros( size(D.run{1}.eeg,2), ceil( length( intervalStim)* P / Q), sum(nTrialsPerBlock));
L = zeros( sum(nTrialsPerBlock),1);
meta.blockID = zeros( sum( nTrialsPerBlock),1);
meta.errProb = zeros( length(D.run),1);
for blockCount = 1:length(D.run)
    eegdat = D.run{ blockCount}.eeg;
    % find relevant events
    [~,evtpos]=ismember( D.run{ blockCount}.header.EVENT.TYP,[5,6,9,10]);
    stimonsets = D.run{ blockCount}.header.EVENT.POS( evtpos>0);
    correctEvent = mod(D.run{ blockCount}.header.EVENT.TYP(evtpos>0),5)==0;

    for ik=1: length( stimonsets)
        curdat = zeros(length(interval),size(eegdat,2));
        curidx = stimonsets( ik)+interval;
        isvalididx = curidx>0 & curidx<=size(eegdat,1);        
        
        % detrend with n=0 equals removing dc shift
        curdat(isvalididx,:) = detrend(eegdat(curidx( isvalididx),:),0);
        
        % remove car
        curdat = curdat - mean( curdat,2);

        % filter 
        filtered = filtfilt( butterB, butterA, curdat);
    
        % do baseline correction for each stim epoch
        baseline = mean(filtered( onset + baselineInterval,:),1);
        epoch = filtered( onset+intervalStim,:) - baseline;
        trialCount = sum( nTrialsPerBlock( 1:blockCount-1))+ik;

        % resample
        epochs(:,:,trialCount) = resample( epoch, P, Q)';
        meta.blockID( trialCount) = blockCount;
        % set label; 1= error, 2 = correct
        L(trialCount) = double( correctEvent(ik))+1;        
    end
    meta.errProb( blockCount) = sum( ~correctEvent)/length( correctEvent);
end
meta.time = linspace(0,winsize,size(epochs,2))*1000;