% This example analyses dataset 2, available at
% http://bnci-horizon-2020.eu/database/data-sets
% # 22. Monitoring error-related potentials (013-2015)
% using cross-validation
% In the experiment, error-related potentials are investigated. Subjects
% observed a cursor movement of an agent
%
% this is a binary single trial classification problem, so we have no
% sequences of stimuli and no targets. Labels are directly calculated in L
% and must not be calculated internally by the ERPDecoder (usually from Y and T)  
function cv = analyze_cv_dataset4( datapath, verbose)

if nargin ==0
    datapath='../../data/dataset4/';
end

if nargin <2
    verbose = true;
end

ComponentLimit = 16; % we limit the number of components to 16 (25% of channels) 

Classifier = 'lda'; % use 'lda' to determine targets
% could also be done with 'svm' 'maxcorr' or 'naivebayes'

FeatureSpace = 'canonical'; % 'maxcorr' classifier requires 'correlation'
% features but other classifiers could also use 'canonical'

ReferenceType = 'average'; % could also be 'impulse' 

flist{1} = dir([ datapath 'Subject*s1.mat']);
flist{2} = dir([ datapath 'Subject*s2.mat']);


if isempty( flist{1}) || length(flist{1})~=length(flist{2})
    error('Not all data files found.');
end

nSubj = length( flist{1});

accuracy = zeros(nSubj,2);
recall = zeros(nSubj,2,2);

for sk = 1: nSubj % loop over subjects
    for sess = 1:2
        dat = load ([ datapath flist{sess}( sk).name]);
    
        % preprocess data: segment, filter and downsample data; define labels 
        [ epochs,L,meta] = preprocess_dataset4( dat);

        % perform classification
        cv = crossvalerpcca( epochs, L, [], [],'10-fold',            ...
            'Alphabet', 'EC',...
            'ComponentLimit', ComponentLimit,...
            'ReferenceType',ReferenceType, ...
            'Classifier', Classifier,...
            'FeatureSpace', FeatureSpace);
        accuracy(sk,sess) = cv.accuracy;
        recall(sk,sess,1) = ...
            sum( cv.prediction=='E' & cv.label=='E') / sum(cv.label=='E');
        recall(sk,sess,2) = ...
            sum( cv.prediction=='C' & cv.label=='C') / sum(cv.label=='C');
        if verbose        
            fprintf('Subj %i Sess %i: %3.0f%% (%3.0f%%/%3.0f%%)\n',...
                sk, sess, accuracy(sk,sess)*100, recall(sk,sess,:)*100);
        end    
    end
end
if verbose
    fprintf( ['Average accuracy sess 1: %3.1f%% (std: %2.1f%%);', ...
        ' sess 2: %3.1f%% (std: %2.1f%%)\n'],...
        mean(accuracy(:,1)*100),std(accuracy(:,1)*100),mean(accuracy(:,2)*100),std(accuracy(:,2)*100));
end