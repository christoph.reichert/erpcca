% [epochs,stimID,trgtID,seqID] = preprocess_dataset3( bciexp)
% preprocesses data of dataset
% https://doi.org/10.5281/zenodo.8188857
% corresponding to publication
% https://dx.doi.org/10.1109/COMPENG50184.2022.9905445

function [epochs,stimID,trgtID,seqID] = preprocess_dataset3(bciexp)
% [epochs,stimID,trgtID,seqID] = preprocess_dataset3(bciexp)

% define parameters
dsf = 5; % downsampling factor, 250Hz/5 = 50Hz
passband = [1, 12.5]; % cutoff frequencies for bandpass filter
interval_length = 0.75; % length of analysis interval in seconds

% bandpass filter coefficients
[butterB, butterA ] = butter( 2, passband / ( bciexp.srate / 2)); 

% determine number of samples per analysis window
nWinSamp = floor( interval_length * bciexp.srate/dsf)-1;

% define interval relative to stimulus onset
interval = 0:nWinSamp-1;

% number of stimuli per trial
nStimPerTrial = sum( diff( sum( bciexp.stim( :, :, 1))) > 0);

% number of available trials
nTrials = size( bciexp.data,3); 

% number of channels
nChan = size( bciexp.data,1); 

% Initialize the matrices for  segmented data and model functions
nStim = nStimPerTrial*nTrials;
epochs = zeros(nChan,nWinSamp,nStim);
stimID = zeros(nStim,1);
trgtID = zeros(nStim,1);
seqID = zeros( nStim,1);

% loop over all trials
for tr=1:nTrials
    
    % filter and resample the data
    rdat = resample( filtfilt( butterB, butterA, bciexp.data( :, :, tr)'), 1, dsf);
        
    % determine stimulus onsets
    stimOnsets = find( bciexp.targetside(1,:,tr)~=0 );
    
    % loop over all stimuli in a trial
    for st = 1: length( stimOnsets)        
        cnt = st+(tr-1)*nStimPerTrial;
        % determine interval
        idx = ceil( stimOnsets( st) / dsf) + interval;
        
        % cut out the resampled data
        epochs(:,:,cnt) = rdat(idx,:)';
        % store the conditions        
        stimID(cnt) = 2-bciexp.stim(1,stimOnsets(st),tr); % 1= green left 2=red left
        trgtID(cnt) = 2-double( strcmp(bciexp.intention{tr},'yes')); %1=Yes; 2=No
        seqID(cnt) = tr;
    end    
end

% it seemed to happen that single stim triggers are missing
missingStim = stimID==0;
if sum( missingStim)>0
%     warning('%i missing stim detected', sum(missingStim));
    epochs(:,:,missingStim) = [];
    stimID(missingStim) = [];
    trgtID(missingStim) = [];
    seqID(missingStim) = [];
end