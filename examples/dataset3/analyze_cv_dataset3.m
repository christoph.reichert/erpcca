% This example analyses dataset 3, available at
% https://doi.org/10.5281/zenodo.8188857
% using cross-validation
% In the experiment green and left light stimuli were presented in the left
% and right visual field. Subjects responded with 'yes' or 'no' by shifting
% their attention to the green and red stimulus, respectively.
function cv = analyze_cv_dataset3( datapath, verbose)

if nargin ==0
    datapath='../../data/dataset3/';% path to the downloaded *.mat files
end

if nargin <2
    verbose = true;
end

Alphabet = 'YN'; % Targets were Yes or No

Contrast = true; % no oddballs but the target was in each stimulus present 
% so we contrast targets and nontargets (to model the difference wave)

ComponentLimit = 3; % we limit the number of components to 3

Classifier = 'maxcorr'; % use maximum correlation to determine tagets
% could also be done with 'svm' 'lda' or 'naivebayes'

FeatureSpace = 'correlation'; % 'maxcorr' classifier requires 'correlation' 
% features but other classifiers could also use 'canonical'

ReferenceType = 'impulse';

% get list of files for all subjects
flist = dir([ datapath, 'P*.mat']);  

if isempty(flist)
    disp('Data not found. Please define the correct data path.');
end

accuracy = zeros(size( flist));

for sk = 1:length(flist)
    % load data structures
    dat = load( [ datapath, flist(sk).name]);
    % preprocess data: segment, filter and downsample data; define labels
    % and targets
    [ epochs,stimID,trgtID,seqID] = preprocess_dataset3( dat.bciexp);
    % perform 12fold cross-validation with folds comprising randomly drawn samples
    cv = crossvalerpcca( epochs, stimID, trgtID, seqID, '12-fold-rand',...
        'Alphabet', Alphabet,...
        'Contrast', Contrast,...
        'ComponentLimit',ComponentLimit,...
        'ReferenceType',ReferenceType, ...        
        'Classifier', Classifier,...
        'FeatureSpace', FeatureSpace);
    accuracy(sk) = cv.accuracy;    
end
if verbose
    fprintf( 'Average accuracy is %3.1f%% (std: %2.1f%%)\n',mean(accuracy*100),std(accuracy*100));
end
