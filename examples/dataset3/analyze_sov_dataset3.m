% This example analyses dataset 3, available at
% https://doi.org/10.5281/zenodo.8188857
% using simulated online validation, i.e.validation is performed as
% reported in the original paper 
% https://dx.doi.org/10.1109/COMPENG50184.2022.9905445
% In the experiment green and left light stimuli were presented in the left
% and right visual field. Subjects responded with 'yes' or 'no' by shifting
% their attention to the green and red stimulus, respectively.

function accuracy = analyze_sov_dataset3( datapath, verbose)

if nargin ==0
    datapath='../../data/dataset3/';% path to the downloaded *.mat files
end

if nargin <2
    verbose = true;
end

Alphabet = 'YN'; % Targets were Yes or No (only required for better 
% interpretation of predictions)

Contrast = true; % no oddballs but the target was in each stimulus present 
% so we contrast targets and nontargets (to model the difference wave)

ComponentLimit = 3; % we limit the number of components to 3

Classifier = 'maxcorr'; % use maximum correlation to determine tagets
% could be also done with 'svm' 'lda' or 'naivebayes'

FeatureSpace = 'correlation'; % 'maxcorr' classifier requires 'correlation' 
% features but other classifiers could also use 'canonical'

ReferenceType = 'impulse';

% get list of files for all subjects
flist = dir([ datapath, 'P*.mat']);  

if isempty(flist)
    disp('Data not found. Please define the correct data path.');
end

accuracy = zeros(size( flist));

for sk = 1:length(flist)

    % load data structures
    dat = load( [ datapath, flist(sk).name]);

    % preprocess data: segment, filter and downsample data; define labels
    % and targets
    [ epochs,stimID,trgtID,seqID] = preprocess_dataset3( dat.bciexp);

    % perform validation as reported in paper
    nTrials = max(seqID);

    % train on first two runs (24 trials) and afterwards include every new trial
    testTrials = 25:nTrials;
    predID = zeros( size( testTrials),'uint8');
    label = zeros( size( testTrials),'uint8');

    % iterate across test trials
    for tr = 1:length(testTrials)
        trainIdx = seqID<testTrials(tr);
        testIdx = seqID==testTrials(tr);
        label(tr) = Alphabet( trgtID(find(testIdx,1)));
        % train the decoder
        Mdl = fiterpcca( epochs(:,:,trainIdx), stimID(trainIdx), trgtID(trainIdx), seqID(trainIdx),...
        'Alphabet', Alphabet,...
        'Contrast', Contrast,...
        'ComponentLimit',ComponentLimit,...
        'ReferenceType',ReferenceType, ...
        'Classifier', Classifier,...
        'FeatureSpace', FeatureSpace);
        % we provide the eeg data and stimuls position to determine the
        % attended target
        predID(tr) = Mdl.predict(epochs(:,:,testIdx),stimID(testIdx),seqID(testIdx));
    end
    accuracy(sk) = sum( predID == label)/length( label);    
end
if verbose
    fprintf( 'Average accuracy is %3.1f%% (std: %2.1f%%)\n',mean(accuracy*100),std(accuracy*100));
end