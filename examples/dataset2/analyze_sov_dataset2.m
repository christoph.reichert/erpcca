% This example analyses dataset 2, available at
% http://bnci-horizon-2020.eu/database/data-sets
% # 19. RSVP speller (010-2015)
% using simulated online validation, i.e.validation is performed as
% reported in the original paper
% In the experiment, a RSVP spelling paradigm is used; 
% only condition color/83ms is present in this data set

function accuracy = analyze_sov_dataset2( datapath, verbose)

if nargin ==0
    datapath='../../data/dataset2/';
end

if nargin <2
    verbose = true;
end

Contrast = ~true; % oddball paradigm, if we contrast targets and nontargets
% common activity (sensory input) is recjected

ComponentLimit = 3; % we limit the number of components to 3 

Classifier = 'lda'; % use 'lda' to determine targets
% could also be done with 'svm' 'maxcorr' or 'naivebayes'

FeatureSpace = 'correlation'; % 'maxcorr' classifier requires 'correlation'
% features but other classifiers could also use 'canonical'

ReferenceType = 'impulse'; % could also be 'average'


flist = dir([ datapath 'RSVP*.mat']);

if isempty( flist)
    error('No data file found.');
end

nSubj = length( flist);

accuracy = zeros(size( flist));

for sk = 1: nSubj % loop over subjects
    dat = load ([ datapath flist( sk).name]);
    % preprocess data: segment, filter and downsample data; define stim 
    % labels and targets
    [ epochs,Y,trgtID,seqID,meta] = preprocess_dataset2( dat);


    % perform classification
    calIdx = meta.blockID == 1;
    intIdx = diff( [ 0; seqID])>0 & meta.blockID > 1;
    intention = meta.alphabet(trgtID( intIdx))';

    % train on calibration block
    Mdl = fiterpcca( epochs(:,:,calIdx), Y(calIdx,:), trgtID( calIdx), seqID( calIdx),...
        'Alphabet', meta.alphabet,...
        'Contrast', Contrast,...
        'ComponentLimit', ComponentLimit,...
        'ReferenceType',ReferenceType, ...
        'Classifier', Classifier,...
        'FeatureSpace', FeatureSpace);
    % predict test blocks (copy spelling and free spelling)
	predID = Mdl.predict(epochs(:,:,~calIdx), Y(~calIdx), seqID( ~calIdx)); 
    accuracy(sk) = sum( predID == intention) / length( intention);

    if verbose        
        fprintf('S%i Intention: %s, Prediction: %s (%3.0f%%)\n',...
            sk, intention', predID',accuracy(sk)*100);
    end
end
if verbose
    fprintf( 'Average accuracy is %3.1f%% (std: %2.1f%%)\n',...
        mean(accuracy*100),std(accuracy*100));
end