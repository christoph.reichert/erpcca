% This example analyses dataset 2, available at
% http://bnci-horizon-2020.eu/database/data-sets
% # 19. RSVP speller (010-2015)
% using cross-validation
% In the experiment, a RSVP spelling paradigm is used; 
% only condition color/83ms is presenmt in this data set

function cv = analyze_cv_dataset2( datapath, verbose)

if nargin ==0
    datapath='../../data/dataset2/';
end

if nargin <2
    verbose = true;
end

Contrast = ~true; % oddball paradigm, if we contrast targets and nontargets
% common activity (sensory input) is recjected

ComponentLimit = 3; % we limit the number of components to 3 

Classifier = 'lda'; % use 'lda' to determine targets
% could also be done with 'svm' 'maxcorr' or 'naivebayes'

FeatureSpace = 'correlation'; % 'maxcorr' classifier requires 'correlation'
% features but other classifiers could also use 'canonical'

ReferenceType = 'impulse'; % could also be 'average'

flist = dir([ datapath 'RSVP*.mat']);

if isempty( flist)
    error('No data file found.');
end

nSubj = length( flist);

accuracy = zeros(size( flist));

for sk = 1: nSubj % loop over subjects
    dat = load ([ datapath flist( sk).name]);
    % preprocess data: segment, filter and downsample data; define stim 
    % labels and targets
    [ epochs,Y,trgtID,seqID,meta] = preprocess_dataset2( dat);

    % perform classification
    cv = crossvalerpcca( epochs, Y, trgtID, seqID,'10-fold-rand',            ...
        'Alphabet', meta.alphabet,...
        'Contrast', Contrast,...
        'ComponentLimit', ComponentLimit,...
        'ReferenceType',ReferenceType, ...
        'Classifier', Classifier,...
        'FeatureSpace', FeatureSpace);
    accuracy(sk) = cv.accuracy;
    if verbose        
        fprintf('S%i Intention: %s, Prediction: %s (%3.0f%%)\n',...
            sk, cv.label', cv.prediction',accuracy(sk)*100);
    end
end
if verbose
    fprintf( 'Average accuracy is %3.1f%% (std: %2.1f%%)\n',...
        mean(accuracy*100),std(accuracy*100));
end