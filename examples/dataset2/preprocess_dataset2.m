function [epochs,stimID,trgtID,seqID, meta] = preprocess_dataset2( D)
% [epochs,stimID,trgtID,seqID, meta] = preprocess_dataset2( D)
% preprocesses data of dataset
% 19. RSVP speller (010-2015)
% from http://bnci-horizon-2020.eu/database/data-sets
% corresponding to publication
% http://dx.doi.org/10.1016/j.clinph.2012.12.050
% only condition color/83ms is present in this data set

filtbuf = 1.0; % buffer pre and post interval to prevent filter edge artifacts
filtorder = 4;
passband = [0.01, 20];
resampfreq = 50;
winsize = 0.8;
baselinelength = 0.116; % analog to paper

meta.alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ_.!<';

eegdat = D.data{1}.X; % data{1}.X and data{2}.X are redundant
blockdur = D.bbci_mrk{2}.T;
fs = D.data{1}.fs;

% filter coefficients
[butterB, butterA] = butter( filtorder/2, passband /(fs/2), 'bandpass');

% resample parameters
[P,Q]=rat(resampfreq / fs);

istarget = [D.data{1}.y == 1, D.data{2}.y == 1]';
% initialize vector of sequence IDs
seqID = [D.data{1}.y_trialIdx, D.data{1}.y_trialIdx(end) + D.data{2}.y_trialIdx]'; 

stimID = [ D.data{1}.y_stim, D.data{2}.y_stim]'; % stimulus

% initialize the vector of target IDs (1-30)
trgtID = zeros(size( stimID)); % the ID of the current target symbol
meta.blockID = zeros(size( stimID)); % a value 1-3

stimonsets = [D.data{1}.trial, D.data{2}.trial];
seqonsets = zeros(length( unique( seqID)) ,1);
seqdur = zeros(length( unique( seqID)) ,1);

for ik=1:length(seqonsets)
    seqonsets(ik) = stimonsets( find(seqID==ik,1));
    seqdur(ik) = stimonsets(find(seqID==ik,1,'last'))-seqonsets(ik);
end


intervalStim = 0:round( winsize * fs);

% initialze epochs (nChan x nSamp x nStim)
epochs = zeros(size(eegdat,2), ceil( length( intervalStim)* P / Q), length( stimonsets));

baselineInterval = -round( baselinelength * fs):0;

blockCount = 1;
for ik=1: length( seqonsets)
    stimeventidx = find(seqID == ik);
    interval = round( -filtbuf * fs): seqdur(ik) + round((winsize+filtbuf) * fs);
    
    % determine target of current spelling trial
    trgtID( stimeventidx) = stimID( find( istarget & seqID == ik, 1));
    % filter the entire stimulus sequence, i.e. one spelling trial
    filteredseq = filtfilt( butterB, butterA, eegdat(seqonsets( ik)+interval,:));

    % cut the sequence into the single stimulus events
    stimOnsetInSeq = stimonsets( stimeventidx)- ...
        stimonsets(stimeventidx(1))- interval(1);
    for k=1:length(stimeventidx)
        % do baseline correction for each stim epoch
        baseline = mean(filteredseq( stimOnsetInSeq(k) + baselineInterval,:),1);
        epoch = filteredseq( stimOnsetInSeq(k)+intervalStim,:) - baseline;
        % resample
        epochs(:,:,stimeventidx(k)) = resample( epoch, P, Q)';
    end
    if stimonsets( stimeventidx(1)) - sum( blockdur(1:blockCount)) > 0
        blockCount = blockCount + 1;
    end
    meta.blockID( stimeventidx) = blockCount;
end
