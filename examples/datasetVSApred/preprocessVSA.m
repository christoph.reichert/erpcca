% [X,Y,T,S,meta] = preprocess_dataset3( bciexp)
% preprocesses data of dataset
% https://doi.org/10.5281/zenodo.14732590
% corresponding to publication
% https://dx.doi.org/TBA

function [X,Y,T,S,meta] = preprocessVSA(session)
% [X,Y,T,S] = preprocess_dataset3(bciexp)

% rereference the EEG to (LMAST+RMAST)/2, RMAST was reference
lmast = find(strcmp(session.eeglabel,'LMAST'));
eegchan = setdiff(1:size(session.eeg,1),lmast);
meta.eegLabel = session.eeglabel( eegchan);
eeg = session.eeg( eegchan,:,:) - session.eeg(lmast,:,:)/2;

% define parameters
dsf = 10; % downsampling factor, 500Hz/10 = 50Hz
passband = [1, 12.5]; % cutoff frequencies for bandpass filter
interval_length = 0.75; % length of analysis interval in seconds

% bandpass filter coefficients
[butterB, butterA ] = butter( 2, passband / ( session.srate / 2)); 

% determine number of samples per analysis window
nWinSamp = floor( interval_length * session.srate/dsf)-1;

% define interval relative to stimulus onset
interval = 0:nWinSamp-1;

% number of stimuli per trial
nStimPerTrial = 10;

% number of available trials
nTrials = size( eeg,3); 

% number of channels
nChan = size( eeg,1); 

% Initialize the matrices for  segmented data and model functions
nStim = nStimPerTrial*nTrials;
X = zeros(nChan,nWinSamp,nStim);
Y = zeros(nStim,1);
T = zeros(nStim,1);
S = zeros( nStim,1);

% keep some meta data
meta.wasFast = false(nStim,1);
meta.wasJittered = false(nStim,1);
meta.wasPredictable = false(nStim,1);
meta.runID = zeros(nStim,1);

% loop over all trials
for tr=1:nTrials
    
    % filter and resample the data
    rdat = resample( filtfilt( butterB, butterA, eeg( :, :, tr)'), 1, dsf);
        
    % determine stimulus onsets
    stimOnsets = find( session.wasRedLeft(:,tr)~=0 );
    
    % loop over all stimuli in a trial
    for st = 1: length( stimOnsets)        
        cnt = st+(tr-1)*nStimPerTrial;
        % determine interval
        idx = ceil( stimOnsets( st) / dsf) + interval;
        
        % cut out the resampled data
        X(:,:,cnt) = rdat(idx,:)';
        % store the conditions        
        Y(cnt) = 2-double( session.wasRedLeft(stimOnsets(st),tr)<0); % 1= green left 2=red left
        T(cnt) = 1+double( session.wasRedTarget(tr)); %1=green; 2=red
        S(cnt) = tr;
        meta.wasFast(cnt) = session.wasFast(tr);
        meta.wasJittered(cnt) = session.wasJittered(tr);
        meta.wasPredictable(cnt) = session.wasPredictable(tr);
        meta.runID(cnt) = session.runID(tr);
    end
end

