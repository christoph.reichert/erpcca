% This example analyses the VSA dataset, available at
% https://doi.org/10.5281/zenodo.14732590
% using cross-validation with custom reference functions
% In the experiment green and left light stimuli were presented in the left
% and right visual field. Subjects shifted
% their attention to the green and red stimulus, respectively.

datapath = '../releasedata/';    % path to the downloaded *.mat files

Alphabet = 'GR'; % Targets were Green or Red
Contrast = true; % we contrast targets and nontargets (to model the difference wave)
ComponentLimit = 3; % we limit the number of components to 3
Classifier = 'maxcorr'; % use maximum correlation to determine tagets
FeatureSpace = 'correlation'; % 'maxcorr' classifier requires 'correlation' 
ReferenceType = 'custom'; % we defin custom reference signals for each time point

% load data structure
dat = load( [ datapath, 'P02.mat']);
% preprocess data: segment, filter and downsample data; define labels
% and targets
[ X,Y,T,S,meta] = preprocessVSA( dat.session);

nTimepoints = size(X,2);

accuracy = zeros(2,nTimepoints);

for k=1:nTimepoints
    fprintf('Analyzing timepoint %i \n',k)    
    tpl{1}=zeros(nTimepoints,1);
    tpl{1}(k)=1; % impulse function with impulse at kth time point for red targets
    tpl{2}=tpl{1}; % impulse function with impulse at kth time point for green targets

    ci = meta.wasFast; %include only faster trials
    cv = crossvalerpcca( X(:,:,ci), Y(ci), T(ci), S(ci), '8-fold-rand',...
        'Alphabet', Alphabet,...
        'Contrast', Contrast,...
        'ReferenceType',ReferenceType, ...        
        'ReferenceSignal',tpl, ...
        'Classifier', Classifier,...
        'FeatureSpace', FeatureSpace);
    accuracy(1,k) = cv.accuracy;    

    ci = ~meta.wasFast; %include only slower trials
    cv = crossvalerpcca( X(:,:,ci), Y(ci), T(ci), S(ci), '8-fold-rand',...
        'Alphabet', Alphabet,...
        'Contrast', Contrast,...
        'ReferenceType',ReferenceType, ...        
        'ReferenceSignal',tpl, ...
        'Classifier', Classifier,...
        'FeatureSpace', FeatureSpace);
    accuracy(2,k) = cv.accuracy;        
end

figure; 
plot(0:1/50:(nTimepoints-1)/50,accuracy);
legend({'faster','slower'});
ylabel('Decoding Accuracy');
xlabel('Time (s)');