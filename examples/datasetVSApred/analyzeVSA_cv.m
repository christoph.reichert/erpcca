% This example analyses the VSA dataset, available at
% https://doi.org/10.5281/zenodo.14732590
% using cross-validation with custom reference functions
% In the experiment green and left light stimuli were presented in the left
% and right visual field. Subjects shifted
% their attention to the green and red stimulus, respectively.

datapath = '../releasedata/';    % path to the downloaded *.mat files

Alphabet = 'GR'; % Targets were Green or Red
Contrast = true; % we contrast targets and nontargets (to model the difference wave)
ComponentLimit = 3; % we limit the number of components to 3
Classifier = 'maxcorr'; % use maximum correlation to determine tagets
FeatureSpace = 'correlation'; % 'maxcorr' classifier requires 'correlation' 
ReferenceType = 'impulse';

% get list of files for all subjects
flist = dir([ datapath, 'P*.mat']);  

if isempty(flist)
    disp('Data not found. Please define the correct data path.');
end

accuracy = zeros(size( flist));

for sk = 1:length(flist)
    % load data structures
    dat = load( [ datapath, flist(sk).name]);
    % preprocess data: segment, filter and downsample data; define labels
    % and targets
    [ X,Y,T,S] = preprocessVSA( dat.session);
    % perform 8fold cross-validation with folds comprising randomly drawn samples
    cv = crossvalerpcca( X, Y, T, S, '8-fold-rand',...
        'Alphabet', Alphabet,...
        'Contrast', Contrast,...
        'ComponentLimit',ComponentLimit,...
        'ReferenceType',ReferenceType, ...        
        'Classifier', Classifier,...
        'FeatureSpace', FeatureSpace);
    accuracy(sk) = cv.accuracy;    
end
fprintf( 'Average accuracy is %3.1f%% (std: %2.1f%%)\n',mean(accuracy*100),std(accuracy*100));
