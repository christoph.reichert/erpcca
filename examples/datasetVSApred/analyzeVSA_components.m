% This example analyses the VSA dataset, available at
% https://doi.org/10.5281/zenodo.14732590
% using cross-validation with custom reference functions
% In the experiment green and left light stimuli were presented in the left
% and right visual field. Subjects shifted
% their attention to the green and red stimulus, respectively.

datapath = '../releasedata/';    % path to the downloaded *.mat files

Alphabet = 'GR'; % Targets were Green or Red
Contrast = true; % we contrast targets and nontargets (to model the difference wave)
ComponentLimit = 3; % we limit the number of components to 3
Classifier = 'maxcorr'; % use maximum correlation to determine tagets
FeatureSpace = 'correlation'; % 'maxcorr' classifier requires 'correlation' 
ReferenceType = 'impulse'; % we defin custom reference signals for each time point

% load data structure
dat = load( [ datapath, 'P02.mat']);
% preprocess data: segment, filter and downsample data; define labels
% and targets
[ X,Y,T,S,meta] = preprocessVSA( dat.session);

accuracy = zeros(1,ComponentLimit);

for k=1:ComponentLimit

    cv = crossvalerpcca( X, Y, T, S, '8-fold-rand',...
        'Alphabet', Alphabet,...
        'Contrast', Contrast,...
        'ComponentLimit',ComponentLimit,...
        'ComponentID', k, ...
        'ReferenceType',ReferenceType, ...        
        'Classifier', Classifier,...
        'FeatureSpace', FeatureSpace);
    accuracy(k) = cv.accuracy;            
end
% get the components
model = fiterpcca( X, Y, T, S,...
        'Alphabet', Alphabet,...
        'Contrast', Contrast,...
        'ComponentLimit',ComponentLimit,...
        'ReferenceType',ReferenceType, ...        
        'Classifier', Classifier,...
        'FeatureSpace', FeatureSpace);
a = model.getChannelPattern;
b = model.getModelSignalWeights;

CH = categorical(meta.eegLabel);
CH = reordercats(CH,meta.eegLabel);
figure;
for k=1:size(a,2)
    subplot(size(a,2),2,(k-1)*2+1);
    plot(0:1/50:(size(b,1)-1)/50,b(:,k));
    xlabel('Time (s)');
    title(sprintf( 'Comp %i; Accuracy: %2.1f%%',k,accuracy(k)));
    subplot(size(a,2),2,(k-1)*2+2);
    barh(CH,a(:,k));
    set(gca,'FontSize',6);
    title(sprintf( 'Comp %i; Pattern Weights',k));
end