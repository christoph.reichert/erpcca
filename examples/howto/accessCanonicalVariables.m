% We consider subject s2 of dataset1 as an example data set to explain the learned model

dat = load('../../data/dataset1/s2.mat');

% preprocess data: segment, filter and downsample data; define stim
% labels and targets
[ epochs, stimID,trgtID,seqID,meta] = preprocess_dataset1( dat);

% time vector for 800ms@64hz
time = linspace( 0, 0.8, ceil(0.8*64));

% Train a ERPCCA model. Here we train on all available trials.
% Note that this can slightly differ from the model trained in a cross validation
% fold (because data were left out for the training)

Mdl = fiterpcca( epochs, stimID, trgtID, seqID,...
    'Contrast', true,...
    'ReferenceType','impulse'); % try also 'average'

% since CCA is performed before Feature space is calculated and classification is
% performed, 'Classifier' and 'FeatureSpace' options are irrelevant

prm=Mdl.getParam; % return the parameters used

Ytempl = Mdl.getTemplates(); % returns the templates used to construct Y, i.e.,
% the model signals by concatenation.
% in case of reference type 'impulse', these are diagonal matrices, in case of
% reference type 'average', the averages across (target) trials and, if contrast is
% set true, nontarget trials (target trial template is last).

A = Mdl.getChannelWeights(); % returns the the spatial filter obtained by CCA.
Ai = Mdl.getChannelPattern();	% returns the pattern induced by the spatial filter.
B = Mdl.getModelSignalWeights(); % returns the weights for model signals obtained by CCA.
Bi = Mdl.getModelSignalPattern(); % returns the pattern induced by the model signal weights.

YX = Mdl.getDataModelProduct(); % returns the product of the (concatenated) model
% signals and channel signals (the data used for CCA): Y'*X
% Using 'impulse' reference functions, this results in average ERP signals,
% if contrast = false, and in difference waves, if contrast = true

% since CCA maximizes correlation w.r.t. A and B, and AX=BY is identical to
% -AX=-BY, the direction of components can be swithed in both A and B to be considered
% for interpretation 

% compose some plots
fH=figure;
subplot(3,3,1);
topo4BNCI003_2015( A(:,1)); % plot first spatial filter
axis square
title('Spatial Filter Weights A(:,1)');

subplot(3,3,2);
topo4BNCI003_2015( A(:,2)); % plot second spatial filter
axis square
title('Spatial Filter Weights A(:,2)');

subplot(3,3,3);
topo4BNCI003_2015( Ai(:,1)); % plot first pattern
axis square
title('Spatial Pattern Ai(:,1)');

subplot(3,3,4);
topo4BNCI003_2015( Ai(:,2)); % plot second pattern
axis square
title('Spatial Pattern Ai(:,2)');

subplot(3,3,5);
% values in each column of B show a time series if ReferenceType is 'impulse', 
% but a spatial filter if ReferenceType is average
if strcmpi( prm.ReferenceType,'impulse')
    plot( time, B(:,1:2)); % plot 2 components
    legend('comp1','comp2','location','SouthEast');
    xlabel('time (s)');
    ylabel('units arbitrary');
    title('Model Signal Weights B(:,1:2)');
else
    topo4BNCI003_2015( B(:,1)); % plot only first component
    axis square
    title('Model Signal Weights B(:,1)');
end


subplot(3,3,6);
if strcmpi( prm.ReferenceType,'impulse')
    plot( time, Bi(:,1:2)); % plot 2 components
    legend('comp1','comp2','location','SouthEast');
    xlabel('time (s)');
    ylabel('units arbitrary');
    title('Model Signal Pattern Bo(:,1:2)');
else
    topo4BNCI003_2015( Bi(:,1)); % plot only first component
    axis square
    title('Model Signal Pattern Bi(:,1)');
end


subplot(3,3,7);
if strcmpi( prm.ReferenceType,'impulse')
    % we divide by the number of target signals, since XY is a sum across
    % target events
    plot( time, YX(:,7)/150); % plot Oz component
    legend('Oz','location','SouthEast');
    xlabel('time (s)');
    ylabel('muV');
else
    topo4BNCI003_2015( YX(:,7)); % plot only first component
    axis square
end
title('Data*Model Product at Oz');

subplot(3,3,8);
% we show the first template; if contrast=true second template is for nontargets
imagesc( Ytempl{end}); 
set(gca,'clim',[-1,1]*max(abs(Ytempl{end}(:))));
title('Model Signal Template (Targets)');
axis square

if prm.Contrast
    subplot(3,3,9);
    % we show the first template; if contrast=true second template is for nontargets
    imagesc( Ytempl{1});
    set(gca,'clim',[-1,1]*max(abs(Ytempl{1}(:))));
    title('Model Signal Template (Nontargets)');
    axis square
end
fH.WindowState='maximized';

function topo4BNCI003_2015(dat)
    elecs = {'Fz','Cz','P3','Pz','P4','PO7','Oz','PO8'};
    epos = [0, 18; 0, 0; -16, -18; 0, -18; 16, -18; -23, -29; 0, -35; 23, -29]/50;
    % draw circle
    plot(sin(-pi:0.1:pi),cos(-pi:0.1:pi),'k');
    hold on
    for k=1:length(elecs)
        text(epos(k,1),epos(k,2)+0.05,elecs{k},'Fontsize',8,'HorizontalAlignment','center');
        text(epos(k,1),epos(k,2)-0.05, sprintf('%1.2f',dat(k)),'Fontsize',8,'HorizontalAlignment','center');
    end
end