% this file shows the framework that is required to implement your own
% classifier. It can be used to implement interfaces to other
% implementations, making them compatibel to the ERPCCA toolbox

classdef customclassifier
    
    properties (Hidden)
        % define properties of your Model here
        ClassNames
        Criterion
    end
    
    methods
        
        function MODEL = customclassifier( X, Y, varargin)
            % X are the training data (number of sample x number of
            % features)
            % Y the class labels, typically numbers
            % varargin should accept at least the argument pair 
            % {'Weight',w}, where w is of size [size(X,1),1] (cf. e.g.,
            % fitcsvm) but it must not necessarily be used
            
            % we need the classnames to know what to predict 
            MODEL.ClassNames = unique(Y);            
            
            % do something with the data to train your model 
            MODEL.Criterion = mean( rand(1,size(X,2))); % only for demonstration purposes
        end
        function [LABEL,SCORE]=predict(MODEL,X)
            % We need the LABEL and a SCORE matrix (cf. e.g., fitcsvm)
            % the LABEL is one of the ClassNames 
            % SCORE is of size [size(X,1), length(MODEL.ClassNames)] and
            % denotes propabilities, i.e. the position with the highest
            % score reveals the label
            
            % determine score (example is only for demonstration purposes)
            SCORE = rand(size(X,1), length(MODEL.ClassNames)) - MODEL.Criterion;
            
            % determine labels
            [~,pos] = max(SCORE,[],2);
            LABEL = MODEL.ClassNames(pos);
        end
    end
end