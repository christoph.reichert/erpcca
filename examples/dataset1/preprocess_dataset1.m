function [epochs,stimID,trgtID,seqID, meta] = preprocess_dataset1( D)
% [epochs,stimID,trgtID,seqID, meta] = preprocess_dataset1( D)
% preprocesses data of dataset 
% 12. Visual P300 speller (003-2015)
% from http://bnci-horizon-2020.eu/database/data-sets
% corresponding to publication
% https://doi.org/10.1016/j.neulet.2009.06.045
% first 2 subjects performed single character paradigm, subject 3 to 10 the
% row/column paradigm
% the order of channels is not documented but a correspondence with the person
% responsible for the data set confirmed the order
% Fz,Cz,P3,Pz,P4,PO7,Pz,PO8

filtbuf = 1.0; % buffer pre and post interval to prevent filter edge artifacts
filtorder = 4;
passband = [0.5, 30]; % [0.5, 30] in paper
resampfreq = 64; % 64 in paper
winsize = 0.8; % 0.8 in paper
baselinelength = 0.116; % 0.116 in paper

% build the 6x6 Matrix as in the experiment
% matrix
spellermatcol = (1:6);
spellermatrow = (1:6)';
spellermatidx = repmat(spellermatcol,6,1) +  (repmat(spellermatrow-1,1,6) .* 6);
meta.alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
% disp( meta.alphabet( spellermatidx)) % displays the matrix

subjstr = fieldnames(D);
eegdat = [ D.(subjstr{1}).train(2:9,:)'; D.(subjstr{1}).test(2:9,:)'];
y_stim = [ D.(subjstr{1}).train(10,:)'; D.(subjstr{1}).test(10,:)'];
istarget = [ D.(subjstr{1}).train(11,:)'; D.(subjstr{1}).test(11,:)'];
blockdur = [size(D.(subjstr{1}).train,2), size( D.(subjstr{1}).test,2)];
fs = 1/ diff(D.(subjstr{1}).train(1,1:2));
% speller could by Row/column or sin gle character stimulation
meta.isRC = max( y_stim) == 12;
    
% filter coefficients
[butterB, butterA] = butter( filtorder/2, passband /(fs/2), 'bandpass');

% resample parameters
[P,Q]=rat(resampfreq / fs);      
      
stimonsets = find( diff(y_stim)>0)+1;
seqonsets = [ stimonsets(1); stimonsets( find( diff( stimonsets)>50)+1)];
    
istarget = istarget( stimonsets);
% initialize vector of sequence IDs
seqID = zeros( length(stimonsets),1); 

% depending on row/column or single character stimulus y_stim is differently
% composed
if meta.isRC
    % if we have a row/column pair stimulus we store each possible
    % letter in stimID
    stimID = zeros(length( stimonsets),6); % for each row/col 6 targets possible
    rc = y_stim( stimonsets);
    isrow = rc > 6;
    iscol = rc < 7;
    stimID(isrow,:) = spellermatidx(rc(isrow)-6,:);
    stimID(iscol,:) = spellermatidx(:,rc(iscol))';
else
    stimID = y_stim( stimonsets); % only one target per stimulus possible
end

% initialize the vector of target IDs (1-36)
trgtID = zeros(size( stimID,1),1); % the ID of the current target symbol
% meta data: to which block corresponds the epoch?
meta.blockID = zeros(size( trgtID)); % a value 1-2
    
% determine the duration of each sequence
seqdur = zeros(length( seqonsets) ,1);
for ik=1:length(seqonsets)
    if ik == length( seqonsets)
        seqoffset = length( y_stim);
    else
        seqoffset = seqonsets( ik+1)-1;
    end
    seqID( stimonsets >= seqonsets(ik) & stimonsets <= seqoffset) = ik;
    seqdur(ik) = stimonsets( find( seqID==ik,1,'last')) - seqonsets(ik);
end
    
intervalStim = 0:round( winsize * fs);    

% initialze epochs (nChan x nSamp x nStim)
epochs = zeros(size(eegdat,2), ceil( length( intervalStim)* P / Q), length( stimonsets));
    
baselineInterval = -round( baselinelength * fs):0;

blockCount = 1;
for ik=1: length( seqonsets)
    stimeventidx = find(seqID == ik);
    interval = round( -filtbuf * fs): seqdur(ik) + round((winsize+filtbuf) * fs);

    % determine target of current spelling trial
    if meta.isRC
        % in row/colums stimulus, determine target from row/col
        % combination
        rc = unique(y_stim( stimonsets( istarget & seqID == ik)));
        if length(rc)~=2
            error('could not determine unique row/column pair');
        end
        trgtID( stimeventidx) = spellermatidx(rc(2)-6,rc(1));

    else
        trgtID( stimeventidx) = stimID( find( istarget & seqID == ik, 1));
    end
    
    % filter the entire stimulus sequence, i.e. one spelling trial
    filteredseq = filtfilt( butterB, butterA, eegdat(seqonsets( ik)+interval,:));
    
    % cut the sequence into the single stimulus events
    stimOnsetInSeq = stimonsets( stimeventidx)- seqonsets(ik)- interval(1);
    for k=1:length(stimeventidx)
        % do baseline correction for each stim epoch
        baseline = mean(filteredseq( stimOnsetInSeq(k) + baselineInterval,:),1);
        epoch = filteredseq( stimOnsetInSeq(k)+intervalStim,:) - baseline;
        % resample
        if P>1
            epochs(:,:,stimeventidx(k)) = resample( epoch, P, Q)';
        else
            epochs(:,:,stimeventidx(k)) = downsample( epoch, Q)';
        end
    end
    if stimonsets( stimeventidx(1)) - sum( blockdur(1:blockCount)) > 0
        blockCount = blockCount + 1;
    end
    meta.blockID( stimeventidx) = blockCount;
end


