% This example analyses dataset 1, available at
% http://bnci-horizon-2020.eu/database/data-sets
% # 12. Visual P300 speller (003-2015)
% using cross-validation
% In the experiment, a 6x6 matrix speller is used; 1 block trained, 1 block
% tested online
% first 2 subjects performed single character paradigm, subject 3 to 10 the
% row/column paradigm
% the order of channels is not documented but a correspondence with the person
% responsible for the data set confirmed the order
% Fz,Cz,P3,Pz,P4,PO7,Pz,PO8

function cv = analyze_cv_dataset1( datapath, verbose)

if nargin ==0
    datapath='../../data/dataset1/';
end

if nargin <2
    verbose = true;
end

Contrast = ~true; % oddball paradigm, if we contrast targets and nontargets
% common activity (sensory input) is recjected

ComponentLimit = 3; % we limit the number of components to 3 

Classifier = 'maxcorr'; % use 'maxcorr' to determine targets
% could also be done with 'svm' 'lda' or 'naivebayes'

FeatureSpace = 'correlation'; % 'maxcorr' classifier requires 'correlation'
% features but other classifiers could also use 'canonical'

ReferenceType = 'average'; % could also be 'impulse'; 'average' might be
% advantageous here because we have much less sensors compared to samples

flist = dir([ datapath 's*.mat']);

if isempty( flist)
    error('No data file found.');
end

nSubj = length( flist);
accuracy = zeros( size( flist)); 
isRC = false( size(flist));

for sk = 1: nSubj % loop over subjects
    dat = load ([datapath filesep flist( sk).name]);
    % preprocess data: segment, filter and downsample data; define stim 
    % labels and targets
    [ epochs,stimID,trgtID,seqID,meta] = preprocess_dataset1( dat);
   
    isRC( sk) = meta.isRC;    

    % perform leave-one-out cross-validation
    cv = crossvalerpcca( epochs,stimID,trgtID,seqID,'leaveout',            ...
        'Alphabet', meta.alphabet,...
        'Contrast', Contrast,...
        'ComponentLimit', ComponentLimit,...
        'ReferenceType',ReferenceType, ...
        'Classifier', Classifier,...
        'FeatureSpace', FeatureSpace);
    accuracy(sk) = cv.accuracy;
    if verbose
        stimstr={'single char stim','row/column stim'};
        fprintf('S%i (%s) Intention: %s, Prediction: %s (%3.0f%%)\n',...
            sk, stimstr{isRC(sk)+1},...
            cv.label', cv.prediction',accuracy(sk)*100);
    end
end
if verbose
    fprintf( 'Average leave-one-out accuracy is %3.1f%% (std: %2.1f%%)\n',...
        mean(accuracy*100),std(accuracy*100));
end