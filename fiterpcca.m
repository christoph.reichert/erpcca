function erpdec = fiterpcca(epochs,stimID,trgtID,seqID,varargin)
%FITERPCCA Fit a classifier that applies CCA-based spatial filtering
%   MODEL=FITERPCCA(epochs,stimID,trgtID,seqID) returns an object MODEL
% that performs spatial filtering using canonical correlation analysis and
% performs classification of stimulus sequences. The approach is only
% applicable when we have two distinct ERPs, usually evoked by target and
% nontarget stimuli. However, due to different temporal locations of target stimuli
% in a sequence, several classes can be discriminated (e.g. in a speller
% paradigm).
% input agrument epochs is a 3-dimensional matrix of size [c,t,n], where c is
% the number of channels, t the number of sample points and n the number of
% epochs. An epoch representing a single trial ERP is commonly consisting of c time
% series following a stimulus which are band-pass filtered and down-sampled to a
% reasonable sampling rate.
% input argument stimID is a column vector of length n or a matrix of size [n,s], 
% where each entry is an event code indicating which stimulus the corresponding epoch 
% belongs to, e.g. in a speller paradigm the letter which was highlighted 
% (only numbers accepted). If several events occur simultaneously, e.g., in
% a matrix speller a whole row is flashed, the s events are provided column-wise 
% in stimID 
% input argument trgtID is a column vector of length n, where each entry
% indicates which was the target stimulus. This is important if a sequence of stimuli 
% is presented during which one of these stimuli is the target stimulus, e.g. in a
% speller paradigm trgtID would be set to the letter which is to be spelled for
% each epoch in this sequence, i.e. also the non-target stimuli (only
% numbers accepted)
% input argument seqID is a column vector of length n, where each entry
% indicates which stimulus sequence the epoch belongs to, e.g. in a speller
% paradigm each stimulus presented when 2nd spelled letter was spelled
% would have entry 2 in seqID.
% Methods: predict
% prediction = MODEL.predict(epochs,stimID,seqID)
% returns predictions made by the model given the data in epochs and the events
% in stimID. seqID indicates the sequence number. The vector predictions returns the
% predicted targets.
%   MODEL=FITERPCCA(epochs,stimID,trgtID,seqID, ...) accepts optional pairs of 
% arguments which specifies properties of the model and methods applied, respectively.
%  'ReferenceType' - 'average' or 'impulse' (default) 
%  defines which type of reference function is used to perform canonical
%  correlation analysis, where 'average' uses the average per condition
%  (target and non-target) as reference functions and 'impulse' constructs
%  one impulse function per sample point in an epoch, i.e. eye(t) as
%  reference functions
%  'Contrast' - 1 (true) or 0 (false) (default)
%  specifies whether reference functions should be applied such that the 
%  difference between conditions is modeling the ERP or whether only the
%  target condition is considered in the model. P300 paradigms usually work
%  well without contrast where lateralized responses like the N2pc work
%  better with contrast
%  'ComponentLimit' - maximum number of components to be kept after CCA (default: 5)
%  'Classifier' - 'lda', 'svm', 'naivebayes' or 'maxcorr' (default)
%  where lda refers to linear discriminant analysis, svm to support vector
%  machine classification and maxcorr determines the maximum correletaion
%  between ERP and canonical data
%  'FeatureSpace' = 'canonical' or 'correlation' (default)
%  defines the feature space to be used for classifiers other than maxcorr
%  'canonical' includes the spatially filtered components as features (i.e. time 
%  series) and 'correlation' includes the canonical correlation values
%  'ClassSizeCorrection' - 0 (false) or 1 (true) (default) applys weight to correct 
%  for different class sizes (supported by all classifiers except maxcorr) 
%  'Alphabet' - a vector of characters (useful for spellers) that are
%  linked to the values in stimID, in this case stimID must have entries from 1 to 
%  number of characters, i.e. stimID refers to the character in the alphabet vector. 
%  When calling MODEL.predict, the character is directly predicted
%  default: no alphabet defined
%  use MODEL=FITERPCCA(epochs,stimID,[],[]...)
%  if the to be predicted targets are not presented in a 
%  sequence of stimuli but single trial (epoch) classification is required. Since 
%  only two classes are accepted with erpcca, only binary classification is possible.  
%  In this case stimID indicates whether the epoch is class 1 or 2 , e.g. target or
%  non-target. With this kind of model call the prediction function with
%  prediction = MODEL.predict(epochs)

if ndims(epochs)~=3
    error('epochs must be of size nChan x nSamp x nTrials.');
end

if size(epochs,3) ~= size(stimID,1)
    error('Size ox epochs does not fit size of stimID');
end

if numel(trgtID)~=length(trgtID) || size(trgtID,1)==1
    error('trgtID must be of size nTrials x 1.');
end

if numel(seqID)~=length(seqID) || size(seqID,1)==1
    error('seqID must be of size nTrials x 1.');
end

erpdec = ERPDecoder(varargin{:});
erpdec = erpdec.fit(epochs,stimID,trgtID,seqID);


end % end of function


