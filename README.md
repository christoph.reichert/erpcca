# ERPCCA

This toolbox is designed to decode event-related potentials (ERP) from EEG/MEG data using canonical correlation analysis (CCA).
Specifically, it decodes targets in a series of target and nontarget stimuli, which is commonly used in brain-computer interfaces (BCI).
The main advantage of this approach is that no channel selection is required, but rather the CCA constructs a spatial filter that automatically combines relevant channels to create new surrogate channels of which only the most informative are used.

## Requirements
The toolbox has been tested with Matlab R2019a and R2021b. Previous versions are not recommended.  
This toolbox requires the  
- **Statistics and Machine Learning Toolbox**   

To execute the example code in addition, the  
- **Signal Processing Toolbox**  
is required for preprocessing steps.

## Getting started

Download the toolbox as zip file and extract it, or clone the repository using your favourite git client.  
In Matlab, add the erpcca folder to the Matlab search path (e.g., addpath D:\git\erpcca).  

The toolbox requires the EEG/MEG data preprocessed as *epochs*, where *epochs* is of size nChannels x nSamples x nStimEvents. 
See help of *fiterpcca.m* and *crossvalerpcca.m* for details.

In *erpcca/examples*, you can find four dataset folders. Each folder includes files that exemplarily show how to use the toolbox on openly available datasets. You can access the function by changing your working directory to the examples folder (e.g., cd D:\git\erpcca\examples\dataset1) or by adding it to the path.  
Call an analysis function by providing the path to the downloaded dataset  
e.g., *cv = analyse_cv_dataset1('D:/data/BNCI_003_2015/');* 

## Fit a decoder model 
The central function to train a model is *fiterpcca*. 

*Mdl=fiterpcca(epochs,st�mID,trgtID,seqID,...)*

*Mdl* is a ERPDecoder object, which we later use to predict classes from unseen data. The following input variables are required: 
- *epochs* - the electrophysiological data of size [*ch*,*ts*,*n*], where *ch* is the number of channels, *ts* is the number of samples in the analysis window, and *n* is the number of epochs (typically cut according to stimulus events),
- *stimID* - the stimulus information, as a matrix of size [*n*,*d*], where *d* is the number of simultaneously shown events (e.g., several letters in a matrix speller). The integer IDs in this matrix indicates the class referring to the event (e.g., [7,8,9,10,11,12], if a row of letters was highlighted in a matrix speller). Please use IDs 1:*c* if you have *c* classes.
- *trgtID* - the current target ID, as a vector of size [*n*,1]. The ID of the current target refers to the numbers in *stimID* (e.g., 7, if class 7 was the target). In a sequence (=trial), all epochs should have the same target ID.
- *seqID* - the current sequence ID, as a vector of size [*n*,1]. In a sequence (= trial), all epochs should have the same sequence ID.

Optionally, you can specify paramaters as name-value pairs. The following parameters can be specified:
- '*ReferenceType*' - 'impulse' (default), 'average' or 'custom'  
'impulse' uses identity matrices, 'average' uses the average across trials as a template to concatenate the matrix of model signals. When set to 'custom', the parameter 'ReferenceSignal' is required as well for providing a custom reference signal  
- '*ReferenceSignal*' - a one or two cell array, where an arbitrary reference signal can be provided. Each cell must contain an array of size [*ts*,*nref*], where  *ts* is the number of samples in the analysis window, and *nref* is the number of reference signals. This argument is only considered when *ReferenceType* is set to 'custom'. If one cell is provided, only target events are involved in the CCA. If two cells are provided, the   first cell should contain the reference functions for nontargets and the second for targets. Note that a negative sign is assigend internally to model difference waves, i.e. defining {eye(ts),eye(ts)} as custom reference signals is identical to setting '*ReferenceType*' to 'impulse' and '*Contrast*' to true.  
- '*FeatureSpace*' - 'correlation' (default) or 'canonical'  
'correlation' calculates the correlation coefficient between canonical brain signals and canonical model signals for each component (i.e., 5 features, if 'ComponentLimit' is 5). In contrast, 'canonical' directly uses the canonical components of the brain signals (i.e., 5*ts features, if 'ComponentLimit' is 5). 
- '*ComponentLimit*' - an integer number (default is 5) to get a fixed number of components or a p-value between 0 and 1 to indicate the significance level, which is used to accept only significant components (e.g., p < 0.05)  
- '*ComponentID*' - a list of components to be selected after CCA. Usually, the first components, as defined with the '*ComponentLimit*' parameter, are selected. By defining this parameter, custom components can be selected from the ranking CCA reveals, e.g., components 2:3 can be selected to test its performance.  
- '*Classifier*' - 'maxcorr' (default), 'naivebayes', 'svm' or 'lda'  
'maxcorr' determines the class achieving maximal correlation features. Therefore, it can only be used with the 'correlation' feature space. The other classifiers use the standard implementation of Matlab's Statistics and Machine Learning Toolbox.
- '*ClassSizeCorrection*' - true (default) or false  
true takes class sizes into account and uses the weight option for classifier training.
- '*Alphabet*' - if not defined as empty (default), a character array of size [1,*c*], where each position denotes a character that is associated with the corresponding stimulus ID (class number). In a speller matrix, this character array would include the letters, which correspond to classes 1:*c*. This option is useful to provide better readability of predictions, e.g., if a word was spelled.
- '*Contrast*' - true or false (default).  
If set to true, model signals are designed, such that nontarget events are subtracted from target signals (or class 1 from class 2), resulting in difference waves. Otherwise, only target events are involved in the CCA, to estimate the spatial filter (which, however, are applied to all epochs for classification).

To obtain the parameters of a trained model, execute:   
*prm = Mdl.getParam();*

## Classify unseen data ##

Once the model is trained, it can be used to predict the target class from test data. This can be easily achieved by  

*predID = Mdl.predict( epochs, stimID, seqID);*  

where *epochs*, *stimID* and *seqID* have the same meaning, as explained above. The output *predID* is the stimulus ID supposed to be the target or the corresponding letter defined in 'alphabet'.

## Perform cross-validation
We provide a cross-validation function, which can be used to estimate the performance of a model on a set of available data.  

*cv = crossvalerpcca( epochs, stimID, trgtID, seqID,selector,...)*  

returns a structure *cv*, with fields: *label* (the cued target class per trial/sequence), *prediction* (the predicted target class per trial/sequence), and *accuracy* (the ratio of correct predictions).  
*epochs*, *stimID*, *seqID* and *trgtID* have the same meaning as explained above.  
*selector* can be a column vector of length *n* (number of epochs) or a string.  
In the vector, for each epoch, a number must be defined to indicate to which fold the
  epoch belongs. Be aware that epochs in a sequence should be included within the same fold. An easier approach is to define a string, such that the function calculates the samples to be included in a fold and leaves out only whole sequences:  
use 'leaveout' as *selector* to perform a leave-one-sequence-out cross-validation;  
use 'k-fold' to perform k-fold cross-validation. Replace k by a number, e.g., '10-fold' for 10-fold cross-validation. By default, k-fold CV selects the sequences ordered as they are defined in S. To perform random selection of sequences to be left out, add the keyword rand to the *selector* string, e.g., '10-fold-rand' to perform 10-fold CV with random selection.  

You can pass through the same optional name-value pairs as with *fiterpcca* (since this function is internally used).
 

## Access the canonical coefficients

To interpret the estimated canonical coefficients and the canonical variates, you can access all important estimates from the trained ERPDecoder model.
- *Mdl.getChannelWeights()* - returns the spatial filter obtained by CCA.
- *Mdl.getChannelPattern()*	- returns the pattern induced by the spatial filter.
- *Mdl.getModelSignalWeights()* - returns the weights for model signals obtained by CCA.
- *Mdl.getModelSignalPattern()* - returns the pattern induced by the model signal weights.
- *Mdl.getDataModelProduct()* - returns the product of the (concatenated) model signals and channel signals. Using 'impulse' reference functions, *Mdl.getDataModelProduct()* results in ERP signals summed across epoichs, if contrast = false, and in difference waves (sum of target epochs minus sum of nontarget epochs), if contrast = true.

## Apply custom classifiers
The naive Bayes, SVM, and LDA classifiers that we implemented were based on Matlab's Statistics and Machine Learning Toolbox implementations. For SVMs, we calculate the parameter 'Boxconstraint' (also known as C) by the approach suggested by Joachims (https://www.cs.cornell.edu/people/tj/svm_light https://doi.org/10.1007/978-1-4615-0907-3) and the regularization parameter in LDA, as suggested by Treder (https://github.com/treder/MVPA-Light https://doi.org/10.3389/fnins.2020.00289). When defining the parameter *Classifier* as a string, no further parameters can be defined.  

However, you can define custom classifiers by calling Matlab implementations, or your own implementation, using function handles.

### Call a classifier using function handles
In order to call a classifier with custom parameters, you can define the *Classifier* argument, which is accepted by *fiterpcca* and *crossvalerpcca*, as a struct with fields: 
- *handle* - a function handle to a classifier function, which is in the form of the fitc*.m functions in the Statistics and Machine Learning Toolbox
- *args* - a cell array of arguments, where two consecutive cells form a string-value pair  
 
For example, to define a k-nearest neighbor classifier using the Mahalanobis distance as the metric, define the *classifier* struct as follows:  

*classifier.handle = @fitcknn*;  
*// fitcknn is included in the Statistics and Machine Learning Toolbox*  
*classifier.args = {'Distance','mahalanobis'};*

Another example is to use SVM with RBF kernel and Hyperparameter Optimization:

*classifier.handle = @fitcsvm;*  
*classifier.args = {'KernelFunction','rbf','OptimizeHyperparameters','auto'};*

Call *fiterpcca* and *crossvalerpcca*, respectively:

*Mdl=fiterpcca( epochs, stimID, trgtID, seqID,'Classifier',classifier,...)*

### Program your own classifier and call it using a function handle

You can program your own classifier and call it through a function handle as described above.  
We provide example code to show the framework of such a custom classifier in *erpcca/examples/custom/customclassifier.m*  
Essentially, it is implemented as an object, which stores the model properties. When the constructor of the class is called, some model is to be trained. Besides the constructor function, which returns the model object, only a *predict* function is required. which returns labels and scores to rank the classification outputs.  
The opportunity to program custom classifier functions is not only advantageous for own algorithm development but can also be used to implement interfaces to classifiers whose input/output is not compatible with the Statistics and Machine Learning Toolbox classifier functions and/or the ERPCCA toolbox.
