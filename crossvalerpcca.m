function cv = crossvalerpcca(epochs,stimID,trgtID,seqID,selector,varargin)

% cv = crossvalerpcca(epochs,stimID,trgtID,seqID,selector,...)
% performs cross validation using ERPDecoder 
% see fiterpcca for arguments epochs,stimID,trgtID,seqID
% if seqID is defined as a scalar, it defines the number of epochs to be pooled
% in the same class (according to stimID, i.e. works only in binary
% classification without targets) Note if seqID is scalar and >1 the classification 
% is not based on single trials but multiple trials/epochs and that the number of
% training data is reduced by a factor of 1/seqID.
% selector can be a string or a column vector of length number of epochs
% in the vector for each epoch a number must be defined to which fold the
% epoch belongs to. Be aware that epochs in a sequence should be included
% within the same fold.
% you can also define a string such that the function cares for leaving out
% only whole sequences:
% use 'leaveout' as selector to perform a leave-one-sequence-out cross-
% validation
% use 'k-fold' to perform k-fold cross-validation. Replace k by a number,
% e.g. '10-fold' for 10-fold cross-validation. By default, k-fold CV
% selects the sequences ordered as they are defined in seqID. To perform random
% selection of sequences to be left out, add the keyword rand to the
% selector string, e.g. '10-fold-rand' to perform 10-fold CV with random
% selection
% (you can use the same options as in fiterpcca.m to parametrize the decoder)

if length(seqID)<2
    if length(seqID)==1 && seqID>1
        seqID = poolEpochs(stimID,seqID);
        if length( unique(seqID))<sqrt(length(stimID))
            error('Check number of Epochs to merge. To less data');
        end
    else
        seqID = (1:length(stimID))';
    end
end

sequences = unique(seqID);
sequenceOnsets = false(size(seqID));
for k=1:length( sequences)
    sequenceOnsets(find(seqID==sequences(k),1)) = true;
end

if length(stimID)~=length(selector) && isnumeric( selector)
    error('selector must have length of stimID containing foldIDs or be a string');
end

if ischar( selector)
    if strcmpi(selector,'leaveout')
            selector = seqID;
    elseif contains( selector,'-fold')
        kf = sscanf( selector,'%i-fold');
        if ~isempty( kf)
            seqsel = ceil( linspace( eps, kf, length( sequences)));
            if kf > length(seqsel)
                error('Not enough data for this k-value')
            end
            if contains( selector,'rand')
                seqsel = seqsel( randperm( length( seqsel))); % randomize
            end
            selector = zeros( size(seqID));
            for k=1:length( sequences)
                % make sure that sequences entirely are kept in one fold
                selector( seqID==sequences(k)) = seqsel( k);
            end
        else
            error('Could not extract k from k-fold');
        end
    else
        error('Selector method not defined.');
    end
end

foldIDs = unique(selector);

alphabetVar = [];
for k=1:length(varargin)
    if strcmpi(varargin{k},'Alphabet')
        alphabetVar = k;
        break;
    end
end
if isempty( alphabetVar)
    alphabet = '';    
else
    alphabet = varargin{alphabetVar+1};    
end

if isempty(alphabet)
    cv.label = zeros( length( sequences),1);
else
    cv.label = repmat( alphabet(1), length( sequences),1);
end

cv.prediction = cv.label;


for k=1:length(foldIDs)
    trainIdx = selector ~= foldIDs(k);
    curIdx = find( ismember( sequences , seqID(~trainIdx & sequenceOnsets)));
    
    if isempty(trgtID)
        if isempty( alphabet)
            cv.label( curIdx) = stimID( ~trainIdx & sequenceOnsets);
        else
            cv.label( curIdx) = alphabet( stimID( ~trainIdx & sequenceOnsets));
        end
        model = fiterpcca( epochs(:,:,trainIdx), stimID(trainIdx,:), [], ...
                           seqID(trainIdx), varargin{:});
        cv.prediction(curIdx) = model.predict(epochs(:,:,~trainIdx),seqID( ~trainIdx));
    else
        if isempty( alphabet)
            cv.label( curIdx) = trgtID( ~trainIdx & sequenceOnsets);
        else
            cv.label( curIdx) = alphabet( trgtID( ~trainIdx & sequenceOnsets));
        end
        model = fiterpcca( epochs(:,:,trainIdx), stimID(trainIdx,:), ...
                           trgtID( trainIdx), seqID( trainIdx),varargin{:});
        cv.prediction(curIdx) = model.predict(epochs(:,:,~trainIdx), ...
                                            stimID(~trainIdx,:), seqID( ~trainIdx));
    end
end
cv.accuracy = sum( cv.prediction == cv.label) / length( cv.label);
end

%% helper functions

function seqID = poolEpochs(stimID,sequencelength)

classes = unique( stimID);

seqID= zeros(size(stimID));

seqcount = 0;
for k=1:length( classes)
    ni = sum(stimID==classes(k));
    int_n = ceil( ni/sequencelength);
    slength = floor(ni/(int_n));
    m1 = ni-(int_n * slength);
    m2 = int_n - m1;
    seq_n = [repmat(slength+1,1,m1), repmat( slength,1,m2)];
    idx = find( stimID==classes(k) & seqID==0);
    cseqcount = 0;
    while ~isempty(idx) 
        seqcount = seqcount + 1;
        cseqcount = cseqcount + 1;
        pidx = randperm( length( idx));
        nextseq = pidx(1:seq_n(cseqcount));
        seqID(idx(nextseq)) = seqcount;
        idx = find( stimID==classes(k) & seqID==0);
    end
end
Sresorted = zeros(size(seqID));
seqcount = 0;
while ~all(Sresorted>0)
    replaceseq = seqID( find(Sresorted==0,1));
    seqcount = seqcount + 1;
    Sresorted(seqID==replaceseq) = seqcount;
end
seqID=Sresorted;
end

