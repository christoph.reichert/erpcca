% type help fiterpcca to make use of the ERPDecoder class 
classdef ERPDecoder
    
    properties

    end
    properties (Hidden)
        param
        templates
        A
        B
        classifier
        isbinary
        
        channelweights
        templateweights
        datamodelproduct
    end
    
    methods
        function this = ERPDecoder( varargin)
            % ERPDecoder() creates ERPDecoder object with default parameters
            %   ReferenceType = 'impulse'; %  'impulse', 'average' or 'custom'
            %   ReferenceSignal = {}; % only considered when ReferenceType
            %   is 'custom', can be one or two cells; in case of two cells,
            %   first is nontarget (negative sign is assigend internally)
            %   ClassSizeCorrection = true; % weight class sizes
            %   ComponentLimit = 5; % number of components to be included
            %   ComponentID = []; % list of components to be selected after 
            %   CCA. Usually the first components as defined with the
            %   ComponentLimit parameter are selected but with this
            %   parameter e.g. components 2:3 can be selected to test its
            %   performance.
            %   Classifier = 'maxcorr'; % 'maxcorr', 'naivebayes', 'svm', 'lda'
            %   FeatureSpace = 'correlation'; % 'correlation' or 'canonical'             
            %   Alphabet = '';
            %   Contrast = false;
            % ERPDecoder('name',value,...) overwrites default parameters, which are 
            % in the list of name-value pairs
            param = args2prm(varargin);
            if ~isstruct( param)
                error('Error in list of argument pairs');
            else
                this.param = param;
            end
            this.isbinary = false;
        end
        function templates = getTemplates(this)
            % getTemplates returns the templates used to build model signals
            templates = this.templates;
        end
        function A = getChannelWeights(this)
            % getChannelWeights returns the spatial filter obtained by CCA
            A = this.channelweights;
        end
        function B = getModelSignalWeights(this)
            % getModelSignalWeights returns the weights for model signals obtained by CCA
            B = this.templateweights;
        end
        function D = getDataModelProduct(this)
            % getDataModelProduct returns the product of the model signals and channel signals#
            % using 'impulse' reference functions, this results in 
            % average ERP if contrast = false and difference waves if contrast = true
            D = this.datamodelproduct;
        end
        function pattern = getChannelPattern(this)
            % getChannelPattern returns the pattern induced by the spatial filter
            invW = pinv( this.A)'; 
            pattern = invW(:,1:size( this.channelweights,2));
        end
        function pattern = getModelSignalPattern(this)
            % getModelSignalPattern returns the pattern induced by the
            % model signal weights
            invW = pinv( this.B)'; 
            pattern = invW(:,1:size( this.templateweights,2));
        end        
        function [dat,lab] = getTrainingData(this,X,Y,T,S)
            % getChannelPattern returns the spatially filtered data and
            % corresponding class labels
            [~,dat,lab] = generateFeatures(this,X,Y,T,S);
        end
        function prm = getParam(this)
            % getParam returns the parameters used to train the model
            prm = this.param;
        end
        %% fit method
        function this = fit (this, X,Y,T,S)
            % fit trains the model
            [this,dat,lab] = generateFeatures(this,X,Y,T,S);
            this.classifier = trainClassifier( dat, lab, this.param);            
        end
        %% prediction
        function T = predict (this, X, YorS, S)
            % predict performs feature extraction and classification of
            % test data and returns the predicted target class
            if nargin <3
                YorS = (1:size(X,3))';
            end
            if nargin <4
                S=YorS;
                if ~this.isbinary
                    error('Too few input arguments. Y is required to predict targets in sequences');
                end
                Y=repmat(this.param.Alphabet(1),length(YorS),1);               
            else
                Y=YorS;
                if this.isbinary
                    error('No Y allowed in binary mode. Call predict(X,S)');
                end
            end
            if ~this.isbinary
                if size(Y,2)>1
                    Y = this.param.Alphabet(Y);
                else
                    Y = this.param.Alphabet(Y)';
                end
            end
            
            sequences = unique(S,'stable');
            nSeqs = length( sequences);
            
            if this.isbinary
                nSymbols = 1;
            else
                nSymbols = length( this.param.Alphabet);
            end
            T = repmat(this.param.Alphabet(1), nSeqs, 1); % initialize T
            
            for sk = 1:nSeqs
                r = zeros(nSymbols,size( this.channelweights,2));
                u = zeros(nSymbols, size(X,2) * size( this.channelweights,2));
                for ak = 1: nSymbols
                    % test for each available symbol
                    curT = this.param.Alphabet(ak);
                    if  this.isbinary
                        curIdx = find( S== sequences(sk));
                    else
                        % only potential targets considered
                        curIdx = find( S== sequences(sk) & any( Y==curT,2));
                    end
                    if ~isempty(curIdx)
                        [Xr,Yr] = getVariableSets(this,...
                            X( :, :, curIdx), Y( curIdx, :) , curT);
                        r(ak,:) = diag( corr(Xr * this.channelweights,...
                            Yr * this.templateweights));
                        for k=1:length( curIdx)
                            curu =  X(:,:,curIdx(k))' * this.channelweights;% / length( curIdx);
                            u(ak,:) =  u(ak,:) + reshape( curu, [1, size( u,2)]);
                        end
                    end
                end
                T( sk) = testClassifier( r, u, this.param, this.classifier);
            end
        end
    end
    methods( Hidden)
        function [this,dat,lab] = generateFeatures(this,X,Y,T,S)
            nTrials = size( X,3); % number of ERP intervals
            
            if nargin < 4 || isempty(S)
                % no stimulus sequence defined -> we assume that a single event represents a sequence
                S=(1:nTrials)';
            end
            if nargin < 3 || isempty(T)
                % No targets defined -> binary mode, the target is defined with Y
                if size(Y,2)>1 || length(unique(Y))~=2
                    error('Y must be a vector comprising two classes if T is not defined.');
                end
                T=ones( nTrials, 1); % in binary mode we assume target label 1
                this.isbinary = true;
            end
            if isempty( this.param.Alphabet)
                % if Alphabet is not defined, the Alphabet is set to the
                % labels in Y
                this.param.Alphabet = unique( Y(:))';
            else
                Y = this.param.Alphabet(Y);
                if size(Y,2)==nTrials && size(Y,1)==1
                    Y=Y';
                end
                T = this.param.Alphabet(T)';
            end
            nSamp = size( X,2); % number of samples in ERP interval
            % determine the sequence IDs and its occurence
            [sequences, occurence] = unique(S,'stable');
            nSeqs = length( sequences);
            TinSeq = T( occurence); % target is the same in an entire sequence
            
            nSymbols = length( this.param.Alphabet);
            if this.isbinary
                % we have only two classes
                targetClass = double( Y==this.param.Alphabet(1)); %assume one class as target
            else
                targetClass = double(any( Y == repmat(T,1,size( Y,2)),2)); % ground truth
            end
            
            if this.param.Contrast %|| this.isbinary
                Ye = [-sum(targetClass==1)/sum(targetClass==0) , 1]; % weight imbalanced
                Te = [0,1]; % non target and target class
            else
                Ye = 1;
                Te = 1;
            end
            
            % determine the templates
            this.templates = cell( length( Ye),1);
            for k=1:length( Ye)
                if strcmp(this.param.ReferenceType,'impulse')
                    this.templates{k} = eye(nSamp) .* Ye(k);
                elseif strcmp(this.param.ReferenceType,'average')
                    this.templates{k} = mean(X(:,:, targetClass==Te(k)),3)' .*Ye(k);
                elseif strcmp(this.param.ReferenceType,'custom')
                    if length(Ye)~=length(this.param.ReferenceSignal)
                        error(['Number of custom reference signals must be'...
                            ' %i (depends on Contrast parameter)'],length(Ye));
                    end
                    if ~iscell(this.param.ReferenceSignal)
                        error(['ReferenceSignal must be a cell array of 1 '...
                            '(if Contrast=false) or 2 (if Contrast = true, '...
                            '2nd cell assumed as target reference signal) cells']);
                    end
                    if size(this.param.ReferenceSignal{k},1)~=nSamp
                        error(['Number of samples in reference signal must match'...
                            ' number of samples in X']);
                    end
                    this.templates{k} = this.param.ReferenceSignal{k} .*Ye(k);
                else
                    error('Reference type undefined.');
                end
            end
            if this.isbinary
                curT = this.param.Alphabet(1);
            else
                curT = T;
            end
            [Xr, Yr] = getVariableSets(this, X, Y, curT);
            if isempty(Xr)
                error('No training data could be extracted. Check input variables.');
            end
            %perform canonical correlation analysis
            if rank(Yr)==size(Yr,2) && ~this.param.Contrast
                % for some reason this setting produces a warning, although the rank is full
                warnStruct = warning('off','stats:canoncorr:NotFullRank');
            else
                warnStruct = warning('on','stats:canoncorr:NotFullRank');
            end
            [ this.A, this.B, ~, ~, ~, STATS] = canoncorr(Xr, Yr);
            warning( warnStruct);
            
            if this.param.ComponentLimit < 1
                nMaxComp = max(1,sum( STATS.p < this.param.ComponentLimit));
            else
                nMaxComp = this.param.ComponentLimit;
            end
            nMaxComp = max(1, min( nMaxComp, size( this.A, 2)));
            
            if isempty( this.param.ComponentID)
                compOfInterest = 1:nMaxComp;
            else
                compOfInterest = intersect(1:nMaxComp, this.param.ComponentID);
            end
            if isempty( compOfInterest)
                error('No components meet the criteria using parameters ComponentID and ComponentLimit');
            end

            this.channelweights = this.A( :, compOfInterest);
            this.templateweights = this.B( :, compOfInterest);
            this.datamodelproduct = (Yr'*Xr);            
            
            % generate feature spaces for classification
            if strcmpi(this.param.FeatureSpace,'correlation')
                % determine correlation for each symbol in a sequence
                % (assuming this is the target symbol)
                dat = zeros( nSeqs * nSymbols, size( this.channelweights,2));
                lab = false( nSeqs * nSymbols, 1);
                count = 0;
                for sk=1:nSeqs
                    for ak = 1:nSymbols
                        curT = this.param.Alphabet(ak);
                        curIdx = find( S== sequences(sk) & any( Y==curT,2));

                        if ~isempty( curIdx)
                            count = count + 1;                            
                            [Xr, Yr] = getVariableSets(this, X(:,:,curIdx), Y(curIdx,:), curT);
                            dat(count,:) = diag( corr( Xr * this.channelweights,...
                                Yr*this.templateweights));
                            lab(count) = this.param.Alphabet(ak) == TinSeq(sk);
                        end
                    end
                end
            elseif strcmpi( this.param.FeatureSpace,'canonical')
                % we use the canonical (virtual) channels as feature space
                dat = zeros(nSeqs*nSymbols, nSamp * size( this.channelweights,2));
                lab = false( nSeqs*nSymbols,1);
                count = 0;
                for sk=1:nSeqs
                    for ak = 1:nSymbols
                        curIdx = find( sequences( sk) == S & any(Y == this.param.Alphabet( ak),2));
                        if ~isempty( curIdx)
                            count = count + 1;
                            for k=1:length( curIdx)
                                u = X(:,:,curIdx(k))' * this.channelweights ;%./length(curIdx);
                                dat(count,:) = dat(count,:) +  reshape( u,[1, size( dat,2)]);
                            end
                            lab(count) = this.param.Alphabet(ak) == TinSeq(sk);
                        end
                    end
                end
            else
                error('Feature space method %s not defined',this.param.FeatureSpace);
            end
            if count < size(lab,1)
                dat = dat(1:count,:);
                lab = lab(1:count);
            end
            if this.isbinary
                lab= ~lab; % in binary mode target is pos 1 given vector [0,1]
            end
        end
    end
end % end ERPCCA

%% internally used functions
function prm = args2prm( args)

if mod(length(args),2) ~=0
    prm = 0;
    return;
end
% define defaults
prm.ReferenceType = 'impulse'; % can be 'impulse', 'average' or 'custom'
prm.ReferenceSignal ={}; % must be defined if ReferenceType is custom
prm.ClassSizeCorrection = true; % considers different number of samples within the classes
prm.ComponentLimit = 5; % number of components to be included
prm.ComponentID = []; % list of components to be selected in the model
prm.Classifier = 'maxcorr'; % maximum correlation, naive bayes, fisher discriminant,...
                            % support vector machine or a struct defining a custom classifier
prm.FeatureSpace = 'correlation'; % correlation values; 'canonical' spatially filtered components

prm.Alphabet = '';
prm.Contrast = false; % default false because P300 are most common and usually don't need to be contrasted

for k=1:2:length( args)
    if ~ischar( args{k})
        error( 'First element of argument pair must be a string');
    end
    if isfield( prm, args{k})
        prm.(args{k}) = args{k+1};
    else
        error( 'Argument %s not defined.', args{k});
    end
end
end

function [Xr, Yr] = getVariableSets(model, X, Y, T)

if numel(T)==1
    T= repmat(T,size(Y,1),1);
end

nChan = size(X,1);
nSamp = size(X,2);
isTarget = double( any( Y==repmat( T, 1, size( Y, 2)), 2));
templates = model.getTemplates();

if length(templates) ==1
    Te = 1;
else
    Te=[0,1];
end

n = zeros( length(templates), 1);
for k=1:length( templates)
    n(k) = sum( isTarget==Te( k));
end

% reshape epochs
Xr = zeros( sum(n)* nSamp, nChan);
Yr = zeros( sum(n)* nSamp, size( templates{1},2) );

count = 0;
for k = 1:length( isTarget)
    if any(Te == isTarget(k))
        count = count + 1;
        idx = (count-1)*nSamp + (1:nSamp);
        Xr(idx,:) = X( :, :,  k)';
        Yr(idx,:) = templates{Te == isTarget(k)};
    end
end

end

function classifier = trainClassifier(dat,lab,param)
% Build the training sets
Xtrain = dat;
Ytrain = double(lab)*2-1;

% normalize Xtrain
classifier.normalize.mu = mean( Xtrain, 1);
Xtrain = bsxfun(@minus, Xtrain, classifier.normalize.mu);
classifier.normalize.sigma = std( Xtrain,[],1);
Xtrain = bsxfun(@rdivide, Xtrain, classifier.normalize.sigma);
% define weights to balance class sizes
weights = ones(size(lab));
if param.ClassSizeCorrection
    weights(~lab) = 1./(sum(~lab)/sum(lab))*ones( sum(~lab),1);
end
if isstruct( param.Classifier) && isa(param.Classifier.handle,'function_handle')
    % arbitrary classifier approaches can be applied here, using matlab's
    % fitc*.m functions or custom functions 
    % ( see example myclassifier.m and readme file)
    classifier.model = param.Classifier.handle( Xtrain, Ytrain, 'Weights',weights,...
                       param.Classifier.args{:});
elseif strcmpi( param.Classifier,'naivebayes')
    classifier.model = fitcnb(Xtrain, Ytrain, 'Weights',weights);
elseif strcmpi( param.Classifier, 'svm')
    % determine C according to Joachims (svmlight) https://www.cs.cornell.edu/people/tj/svm_light/
    C=1/mean(diag(Xtrain*Xtrain'));
    classifier.model = fitcsvm(Xtrain, Ytrain, 'Weights',weights, 'Boxconstraint',C);
elseif strcmpi( param.Classifier, 'lda')
    % determine lambda according to LedoitWolf (inspired by
    % MVPA-light) https://github.com/treder/MVPA-Light
    S = Xtrain'*Xtrain / size(Xtrain,1);
    X2 = Xtrain.^2;
    phi = sum( sum( X2'*X2 / size(X2,1) - S.^2));
    gamma = norm(S - trace(S)*eye(size(S)) / size(X2,2),'fro')^2;
    lambda = max(0, min((phi/gamma)/ size( X2,1), 1));
    classifier.model = fitcdiscr( Xtrain, Ytrain,  'Weights',weights, 'Gamma', lambda);
elseif strcmpi( param.Classifier, 'maxcorr')
    % dat is the correlation of the canonical components
    d{1} = mean(dat(lab==0,:),2); % nontargets
    d{2} = mean(dat(lab==1,:),2); % targets
    % determine the optimal threshold
    [~,pos] = max([mean(d{1}), mean(d{2})]);
    x=sort( mean(dat,2));
    acc = zeros(size( x));
    for k=1:length(x)
        acc(k) = (sum(d{3-pos}<x(k))+sum(d{pos}>=x(k)))/length(x);
    end
    confidencethresh = quantile(acc,.95);
    classifier.maxcorr.threshold = mean( x( acc>=confidencethresh));
else
    error(['Classifier method not defined. Accepted methods: ',...
        'maxcorr, naivebayes, lda, svm (or a struct defining a custom classifier)']);
end
end

function T = testClassifier( r,u, param, classifier)
if ~isstruct( param.Classifier) && strcmpi(param.Classifier,'maxcorr')
    r_mean = mean( r,2)-classifier.maxcorr.threshold;
    if numel(r_mean) ==1
        pos = 2-double(r_mean>0); % in binary mode target is pos 1 given vector [0,1]
    else
        [~, pos] = max( r_mean);
    end
    T = param.Alphabet( pos);
    return;
end
if strcmpi(param.FeatureSpace,'correlation')
    Xtest = r;
elseif strcmpi( param.FeatureSpace,'canonical')
    % we use the canonical (virtual) channels as feature space
    Xtest = u;
else
    error('Feature space method %s not defined',param.FeatureSpace);
end
% normalize
Xtest = bsxfun(@minus, Xtest, classifier.normalize.mu);
Xtest = bsxfun(@rdivide, Xtest, classifier.normalize.sigma);
% classify
[ T, posterior] = predict( classifier.model, Xtest);
if size( Xtest,1)==length( param.Alphabet)
    [ ~, pos] = max( posterior(:,2),[],1);
else
    pos = T/2+3/2; % convert [-1 1] to [1 2]
end
T = param.Alphabet( pos);
end